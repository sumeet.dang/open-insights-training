import requests
import json
import sys

if __name__ == "__main__":
    srcSystemEp = 'http://localhost:10001/api/v1/srcSystem'
    result = requests.get(srcSystemEp)
    if(result.status_code != 200):
        print("Request Failed, exiting Script")
        sys.exit(1)
    else:
        r = result.json()
    
    for source in r:
        srcId = source['srcSystId']
        srcName = source['srcSystName']
        connType = source['connectionType']
        dbName = source['connectionDetails']['dbName']
        print("{},{},{},{}".format(srcId, srcName, connType, dbName))
    
    
    