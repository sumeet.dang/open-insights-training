# -*- coding: utf-8 -*-
#change api
# for loop 

import requests
import json
import sys

if __name__ == "__main__":
    rulesEp = 'http://localhost:10001/api/v1/rules'
    result = requests.get(rulesEp)
    if(result.status_code != 200):
        print("Request Failed, exiting Script")
        sys.exit(1)
    else:
        r = result.json()
    
    
    for source in r:
        ruleId = source['ruleId']
        versionId = source['versionId']
        correlationId = source['correlationId']
        ruleType = source['ruleType']
        ruleDesc = source['ruleDesc']
        dimension = source['dimension']
        attributeIds = source['entityId']['attributeIds']
        print("{},{},{},{},{},{},{}".format(ruleId, versionId, correlationId, ruleType,
              ruleDesc,dimension,attributeIds))


