import requests
import json
import sys
import pandas as pd

if __name__ == "__main__":
    attributesPing = requests.get("http://localhost:10001/api/v1/attributes")
    print(attributesPing.status_code)
    if attributesPing.status_code != 200:
        print("not able to ping the API")
        sys.exit(1)
    else:
        r = attributesPing.json()
        
# for response in r:
#     attributeId = response['attributeId']
#     entityId = response['entityId']
#     attributeName = response['attributeName']


# print(attributeId)
df = pd.json_normalize(r)
print(df.head(10))