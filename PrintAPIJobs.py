import requests
import json
import sys

if __name__=="__main__":
    jobsAPI="http://localhost:10003/api/v1/jobs"
    result = requests.get(jobsAPI)

    if(result.status_code != 200):
        print('Request failed,exiting script')
        sys.exit(1)
    else:
        r=result.json()

    for jobsy in r:
            jobId = jobsy['jobJson']['jobId']
            interval = jobsy['jobJson']['interval']
            ruleIds = jobsy['jobJson']['ruleIds']
            sourceDetails = jobsy['jobJson']['sourceDetails']
            print("{},{},{},{}".format(jobId, interval, ruleIds, sourceDetails))