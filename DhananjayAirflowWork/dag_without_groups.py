from airflow import DAG
from airflow.operators.empty import EmptyOperator
from datetime import datetime
from airflow.utils.task_group import TaskGroup


with DAG(
            dag_id="dhananjay_DAGs",
            schedule_interval=None,
            start_date=datetime(2022,1,1)
            ) as dag:
    task1 = EmptyOperator(task_id="test_task1")
    task2 = EmptyOperator(task_id="test_task2")
    task3 = EmptyOperator(task_id="test_task3")
    task4 = EmptyOperator(task_id="test_task4")
    task5 = EmptyOperator(task_id="test_task5")


    task1 >> task2 >> task3 >> task4 >> task5