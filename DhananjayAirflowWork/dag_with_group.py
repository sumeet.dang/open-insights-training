from airflow import DAG
from airflow.operators.empty import EmptyOperator
from datetime import datetime
from airflow.utils.task_group import TaskGroup

def func1():
    print("Function one is running")

def func2():
    print("Function 2 is running")

with DAG(
            dag_id="Dhananjay_DAG_Group",
            schedule_interval=None,
            start_date=datetime(2022,1,1)
            ) as dag:
    task1 = EmptyOperator(task_id="test_task1")

    with TaskGroup('group1') as grp1:
        task2 = EmptyOperator(task_id="test_task2")
        task3 = EmptyOperator(task_id="test_task3")
        task2 >> task3
    with TaskGroup('group2') as grp2:
        task4 = EmptyOperator(task_id="test_task4")
        task5 = EmptyOperator(task_id="test_task5")
        task4 >> task5

    task1 >> grp1 >> grp2